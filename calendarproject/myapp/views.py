from django.shortcuts import render
from django.http import HttpResponse
from .models import CalendarItem

def index(req):
    items = CalendarItem.objects.all()
    return render(req,'myapp/index.html',{"items" : items})